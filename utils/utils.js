const getImcAvg = (array)=>{
  let result = []
  array.forEach(arr=>{
    let data = arr.data

    result.push(data.weight/data.height)
  })

  return result.reduce((a, b) => a + b, 0) / result.length
}

const getMedian = (array) =>{
  
  let middle = Math.floor(array.length / 2);
  array = [...array].sort((a, b) => a.data.height - b.data.height);
  
  let median = array.length % 2 !== 0 ? array[middle] : (array[middle - 1] + arr[middle]) / 2

  return median.data.height

}

const getTopCountries = (array) => {
  let countries = []
  array.forEach(arr=>{
    let score = arr.data.last.reduce((a, b) => a + b, 0)
    if(!countries.filter(country=>country.label == arr.country.code).length>0)
      countries.push({label: arr.country.code, score: score})
    else{
      countries.map(obj => {
        if (obj.label == arr.country.cod) {
          let newScore = score+obj.score
          return {...obj, score: newScore};
        }
      
        return obj;
      });
    }

  })
  
  countries = countries.reduce(function(prev, current) {
    return (prev.score > current.score) ? prev : current
  })

  return countries.label

}

module.exports = {
  getImcAvg,
  getMedian,
  getTopCountries,
}