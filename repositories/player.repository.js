const fs = require('fs');
const db = 'data/headtohead.json'

class FileRepository{
  #file
  constructor(){
    this.#file = db
  }

  // recuperation de nos données
  async #read(){
    return JSON.parse(fs.readFileSync(this.#file).toString()).players
  }
  
  //recuperation de toutes les données
  async all(){
    let result = await this.#read()

    return result
  }

  //recuperation d'une donnée
  async find(id){
    let output = await this.#read()
    return output.find(out=>out.id == id)
  }

}

module.exports = FileRepository