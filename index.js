require('dotenv').config();

const express = require('express')
const cors = require("cors")
const app = express()
const { getMedian, getImcAvg, getTopCountries } = require("./utils/utils")

const { PORT=8080, LOCAL_ADDRESS='0.0.0.0' } = process.env

const PlayerRepository = require('./repositories/player.repository')
const playerRepository = new PlayerRepository()

app.use(cors({
  credentials: true
}));

app.listen(PORT, LOCAL_ADDRESS, ()=>console.log(`Server running  on port ${PORT}...`))

app.use(express.json({extended: true }))
app.use(express.urlencoded({ extended: true}))

app.get("/api", async (req, res)=>{

  res.status(200).send("Hello peeps!")
})

app.get("/api/player", async (req, res)=>{
  playerRepository.all()
  .then(rs=> res.status(200).json(rs.sort((a,b) => b.data.points - a.data.points)))
  .catch(err=> res.status(500))
})

app.get("/api/player/stats", async (req, res)=>{
  playerRepository.all()
  .then(rs=>{

    res.status(200).send({
      countries: getTopCountries(rs),
      imc: getImcAvg(rs),
      median: getMedian(rs)
    })
  })
  .catch(err=>{ 
    res.status(500)
    console.log(err)
  })
})


app.get("/api/player/:id", async (req, res)=>{
  playerRepository.find(req.params.id)
  .then(rs=>res.status(200).json(rs))
  .catch(err=> res.status(404).send({error: "Player not found"}))
})
